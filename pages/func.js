const url_thumb = "./thumb";
const repoUrl = "https://gitlab.com/cfd-pizca/sandbox/-/tree/master/";
const noImageUrl = "./missing.svg";

// Function to load JSON file
async function loadJSON(filename) {
    const response = await fetch(filename);
    const node_data = await response.json();
    return node_data;
}

function changeFileExtension(filePath, newExtension) {
    // Check if newExtension starts with a dot, if not, add it
    if (newExtension[0] !== '.') {
        newExtension = '.' + newExtension;
    }

    // Find the position of the last dot in the filePath
    const lastDotPosition = filePath.lastIndexOf('.');

    // If no dot is found, append the new extension
    if (lastDotPosition === -1) {
        return filePath + newExtension;
    }

    // Otherwise, replace the existing extension with the new one
    return filePath.substring(0, lastDotPosition) + newExtension;
}

function getFileExtension(filePath) {
    // Find the position of the last dot in the filePath
    const lastDotPosition = filePath.lastIndexOf('.');

    // If no dot is found, return an empty string (no extension)
    if (lastDotPosition === -1) {
        return '';
    }

    // Otherwise, return the substring from the last dot to the end of the string
    return filePath.substring(lastDotPosition + 1);
}

// Function to add hover effect to blocks
async function addHoverEffect(svg) {
    const node_images = await loadJSON("images.json");
    const node_data = await loadJSON("node_data.json");
    const blocks = svg.querySelectorAll('.node');

    blocks.forEach(block => {
        const nodeId = block.getAttribute("id");

        let imgSrcs = [noImageUrl];
        if ( (nodeId in node_images) &&
             ("images" in node_images[nodeId]) &&
             node_images[nodeId]["images"] != [] ) {
            imgSrcs = node_images[nodeId]["images"];
        }

        if ( !(nodeId in node_data) || !("url" in node_data[nodeId]) ) {
            node_data[nodeId] = {};
            node_data[nodeId]["url"] = repoUrl + "/cases/" + nodeId;
        }

        block.addEventListener('click', () => {
            const preview = document.querySelector('.preview');
            const sidebar = document.querySelector('.sidebar');
            sidebar.classList.add('visible');
            preview.innerHTML = '';

            const blockHtml = block.innerHTML.trim();
            const textDiv = document.createElement('div');
            const preview_title = document.createElement('h3');
            const anchor = document.createElement('a');
            const description = document.createElement('div');
            anchor.href = node_data[nodeId]["url"];
            anchor.classList.add('preview-title');
            preview_title.appendChild(anchor);
            description.classList.add('description');

            Array.from(block.querySelectorAll('text')).forEach(textElement => {
                Array.from(textElement.children).forEach(child => {
                    const p = document.createElement('p');
                    if (child.textContent.endsWith('/') && anchor.innerHTML.trim() == "") {
                        anchor.innerText = child.innerText || child.innerHTML;
                    } else if ( child.innerHTML.trim() != "" ) {
                        p.innerHTML = child.innerHTML;
                        description.appendChild(p);
                    } } ); } );
            textDiv.appendChild(preview_title);
            textDiv.appendChild(description);
            preview.appendChild(textDiv);

            imgSrcs.forEach(imgSrc => {
                const img = document.createElement('img');
                if (imgSrc == noImageUrl) {
                    img.src = imgSrc;
                } else {
                    if ( getFileExtension(imgSrc) == "ogv" ) {
                        img.src = url_thumb + '/' + nodeId + '/' + changeFileExtension(imgSrc, "gif");
                    } else {
                        img.src = url_thumb + '/' + nodeId + '/' + imgSrc;
                    }
                }
                img.classList.add('node-image');
                img.addEventListener('click', () => {
                    // console.log('Image clicked: ', img.src);
                    window.open(
                        node_data[nodeId]["url"] + '/' + imgSrc,
                        '_blank');
                });
                preview.appendChild(img);
            });

            // // Recentering the map to the clicked block
            // const blockBBox = block.getBBox();
            // const blockCenter = [
            //     blockBBox.y + blockBBox.height / 2,
            //     blockBBox.x + blockBBox.width / 2   ];
            // const blockLatLng = map.layerPointToLatLng(L.point(blockCenter));
            // map.setView(blockLatLng);
        });

        block.addEventListener('mouseover', (event) => {
            const rp = block.querySelector('rect') || block.querySelector('path') || block;
            rp.classList.add('hover-effect');
        });

        block.addEventListener('mouseout', () => {
            const rp = block.querySelector('rect') || block.querySelector('path') || block;
            rp.classList.remove('hover-effect');
        });
    });
}

// Function to set IDs for specific <g> elements
function setIdBasedOnTspanText(svgElement) {
    const gElements = svgElement.querySelectorAll('g');

    gElements.forEach(g => {
        const children = g.children;

        Array.from(children).forEach(child => {
            if (child.tagName === 'text') {
                const tspanChildren = child.children;

                Array.from(tspanChildren).forEach(tspan => {
                    if (tspan.tagName === 'tspan' && tspan.textContent.endsWith('/')) {
                        const id = tspan.textContent.slice(0, -1);
                        g.id = id;
                        g.classList.add('node');
                    }
                });
            }
        });
    });
}

// Create a map
var map = L.map('map', {
    crs: L.CRS.Simple,
    maxZoom: 8,
    minZoom: 5
} );

// Define the bounds of the SVG image (adjust according to the SVG's viewBox)
var southWest = L.latLng(-10, -10),
    northEast = L.latLng( 10, 1  ),
    bounds = L.latLngBounds(southWest, northEast);

function reloadSvg() {
    const svgElement = document.querySelector('img[src*="dia.svg"]');
    const newSrc = `dia.svg?v=${new Date().getTime()}`;
    svgElement.src = newSrc;
}

// Call the function to force reload the SVG
reloadSvg();

// Fetch the SVG file as plain text
fetch('map.svg')
    .then(response => response.text())
    .then(svgContent => {
        // Create an SVG element from the string content
        var parser = new DOMParser();
        var svgElement = parser.parseFromString(svgContent, "image/svg+xml").documentElement;

        // Create a custom SVG overlay
        var svgOverlay = L.svgOverlay(svgElement, bounds, {
            interactive: true,
            attribution: `<a href="${repoUrl}">Main code repository</a>`
        }).addTo(map);

        // Fit the map to the bounds of the SVG image
        map.fitBounds(bounds);

        // Set IDs for specific <g> elements
        setIdBasedOnTspanText(svgElement);

        // Add hover effect to blocks
        addHoverEffect(svgElement);

        map.setView(
            [1.9, -3.15],
            7);
    })
    .catch(error => console.error('Error loading the SVG file:', error));

// Close sidebar on mobile
document.getElementById('close-sidebar').addEventListener('click', function() {
    document.querySelector('.sidebar').classList.remove('visible');
});

document.addEventListener('DOMContentLoaded', () => {
    const helpButton = document.getElementById('show-help');
    const helpMessage = document.getElementById('help-message');
    const helpSmallMessage = document.getElementById('help-small-message');

    helpButton.addEventListener('click', () => {
        if (helpMessage.style.display === 'none' || helpMessage.style.display === '') {
            helpMessage.style.display = 'block';
            helpSmallMessage.style.display = 'none';
        } else {
            helpSmallMessage.style.display = 'block';
            helpMessage.style.display = 'none';
        }
    });
});
