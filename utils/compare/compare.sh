
# This script is intended to be run from the "cases" dir.
[ "$(basename $PWD)" == "cases" ]  || exit 1

if [ "$#" == "2" ]
then
    first=$1
    second=$2
else
    echo
    echo "Available cases are:"
    echo
    ls

    echo
    echo "-----------------------"
    echo "Please choose cases to compare (you can use the tab completion)"

    read -e -p "First: " first
    read -e -p "Second: " second

    [ -d "$first" ] && [ -d "$second" ] || {
        echo
        echo "Please select from the avalable options"
        exit 3
    }
fi

echo
echo "-----------------------"

# This works fine, but if piped to less it has no color.
# diff -u -r --color \
#      --exclude "*.pvsm" \
#      --exclude "*.png" \
#      --exclude "*.org" \
#      $first $second

origdir=$PWD
tmpdir=$(mktemp -d)
rootdir=$(git rev-parse --show-toplevel)
git clone "$rootdir" "$tmpdir/repo"
# ls -lha "$tmpdir/repo"
find "$tmpdir/repo" -iname "*.org" -delete
find "$tmpdir/repo" -iname "*.png" -delete
find "$tmpdir/repo" -iname "*.pvsm" -delete
cd "$tmpdir/repo/cases"
git diff --ignore-all-space --no-index -- $first $second

echo
echo "-----------------------"

read -r -p "Dump this to a file [y/N]? " response
response=${response,,} # tolower
if [[ $response =~ ^(n| ) ]] || [[ -z $response ]]; then
    echo
else
    git diff --ignore-all-space --no-index -- $first $second \
        > "$origdir/comparisson_$(date +%s).diff"
    echo "DUMP: $origdir/comparisson_$(date +%s).diff"
fi

cd $origdir
rm -rf "$tmpdir"

echo
echo "-----------------------"

read -r -p "Launch meld [y/N]? " response
response=${response,,} # tolower
if [[ $response =~ ^(n| ) ]] || [[ -z $response ]]; then
    echo
else
    echo
    echo "-----------------------"

    # Check that meld is present
    command -v meld >/dev/null 2>&1 || {
        echo >&2 "Please install meld.  Aborting."
        exit 2
    }

    meld $first $second
fi
