#!/bin/bash
#

# Run from this directory
cd "${0%/*}" || exit

# Directory to scan
base_dir="../cases"

# Output JSON file
output_file="../pages/images.json"

# Image extensions to search for
# (ogv files are thumbnailed as gifs)
imgext="png ogv mp4 jpg"

# Convert space-separated extensions into a find expression
find_expr=$(echo "$imgext" | sed 's/ / -o -name *./g')

# Start the JSON structure
echo "{" > "$output_file"

# Iterate over each directory in the base directory
for dir in "$base_dir"/*/; do
    # Get the case name (directory name without path)
    case_name=$(basename "$dir")

    # Start the case object
    echo "    \"$case_name\": {" >> "$output_file"
    echo "        \"images\": [" >> "$output_file"

    # Find all images with specified extensions in the directory
    images=$(find "$dir" -maxdepth 1 -type f \( -name "*."$find_expr \) -printf "%f\n" | sort)

    # Add images to the JSON array
    first=true
    while IFS= read -r img_name; do
        [ -z "$img_name" ] && continue
        if [ "$first" = true ]; then
            first=false
        else
            echo "," >> "$output_file"
        fi

        # Add the image file name to the JSON array
        echo -n "            \"$img_name\"" >> "$output_file"
    done <<< "$images"

    # End the images array and the case object
    echo "" >> "$output_file"
    echo "        ]" >> "$output_file"
    echo "    }," >> "$output_file"
done

# Remove the trailing comma from the last case object
truncate -s-2 "$output_file"

# End the JSON structure
echo "" >> "$output_file"
echo "}" >> "$output_file"
