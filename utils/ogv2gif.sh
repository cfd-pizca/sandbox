#!/bin/bash
#

if [ "$1" == "--thumbnail" ]
then
    scale="scale=300:-1:flags=lanczos,"
    shift 1
else
    scale=""
fi

[ -f "$1" ] || exit 1

input_file="$1"
input_dir=$(dirname "$input_file")
filename=$(basename -- "$input_file")
extension="${filename##*.}"
filename="${filename%.*}"

outfile="$input_dir/$filename.gif"
fps=5

# Function to create the GIF
create_gif () {
    ffmpeg -y -i "$input_file" \
           -filter_complex \
           "fps=$1,${scale}split[s0][s1];[s0]palettegen=max_colors=32[p];[s1][p]paletteuse=dither=bayer" \
           "$outfile"
}

# Create the initial GIF
create_gif $fps

# Check the size of the GIF file
filesize=$(stat -c%s "$outfile")

if [ "$1" == "--thumbnail" ]
then
    # If the GIF is larger than 500 kB, iteratively reduce the number of frames by half
    while [ $filesize -gt 500000 ]; do
        fps=$(echo "$fps / 2" | bc -l)
        create_gif $fps
        filesize=$(stat -c%s "$outfile")
    done
fi
