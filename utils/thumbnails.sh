#!/bin/bash
#

# Run from this directory
cd "${0%/*}" || exit

INPUT_DIR="../cases"
OUTPUT_DIR="../pages/thumb"

[ -d "$INPUT_DIR" ] || exit 1

rm -rf "$OUTPUT_DIR"
mkdir -p "$OUTPUT_DIR"

THUMBNAIL_SIZE="300x>"

# Function to create thumbnails for PNG files
process_png() {
    local file=$1
    local relative_path=$2
    local file_dir=$3

    mkdir -p "$OUTPUT_DIR/$file_dir"
    convert -strip -thumbnail "$THUMBNAIL_SIZE" "$file" "$OUTPUT_DIR/$relative_path"
}

# Function to process GIF files
process_gif() {
    local file=$1
    local relative_path=$2
    local file_dir=$3
    local filesize

    mkdir -p "$OUTPUT_DIR/$file_dir"
    filesize=$(stat -c%s "$file")

    mv "$file" "$OUTPUT_DIR/$relative_path"

    # if [ $filesize -lt 500000 ]; then
    #     cp "$file" "$OUTPUT_DIR/$relative_path"
    # else
    #     convert "$file" -coalesce -delete 0--2 -resize "$THUMBNAIL_SIZE" -gravity center -pointsize 40 -draw "text 0,0 'GIF'" "$OUTPUT_DIR/$relative_path"
    # fi
}

# Iterate over OGV files and call ogv2gif.sh if no corresponding GIF exists
for file in $(find "$INPUT_DIR" -maxdepth 2 -type f -name "*.ogv"); do
    echo "$file"

    gif_file="${file%.ogv}.gif"
    if [ ! -f "$gif_file" ]; then
        ./ogv2gif.sh --thumbnail "$file" > /dev/null 2>&1
        if [ "$?" -eq 0 ]; then
            echo "$gif_file"
        fi
    fi
done

# Find and process PNG and GIF files
find "$INPUT_DIR" -maxdepth 2 -type f \( -name "*.png" -o -name "*.gif" \) | while read -r file; do
    echo $file

    relative_path="${file#$INPUT_DIR/}"
    file_dir=$(dirname "$relative_path")

    case "${file##*.}" in
        png)
            process_png "$file" "$relative_path" "$file_dir"
            ;;
        gif)
            process_gif "$file" "$relative_path" "$file_dir"
            ;;
    esac
done
